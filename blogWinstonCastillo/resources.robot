***Settings***
Library     SeleniumLibrary
Library     String

***Variables***
${Browser}  chrome
${Homepage}    blog.winstoncastillo.com/category/qa-testing/
${Page}  http://winstoncastillo.com/udemy/
${Url}   http
${SchemeUrl}      ${Url}://${Homepage}

***Keywords***
Open Blog
    Open Browser    ${SchemeUrl}    ${Browser}

Open Homepage
    Open Browser    ${Page}     ${Browser}

