***Settings***

Resource   resources.robot

***Test case***
001 Ir al blog de Winston Castillo
    Open Blog
    Title Should Be    QA Testing – Winston Castillo
    Click Link   xpath=//*[@id="recent-posts-2"]/ul/li[1]/a
    Title Should Be     VLOG: 10 Consejos Para Mantenerte Trabajando en tus Proyectos Personales – Winston Castillo
    Close Browser

002 Ir al blog de Winston Castillo
    Open Homepage
    Title Should Be      Hola Mundo!
    Set Focus To Element    xpath=/html/body/div[1]/div/img
    Click Link      xpath=/html/body/div[1]/div/div[2]/a[1]
    Title Should Be    Winston Castillo – Un sitio para comunicarse
    Close Browser

003 Abrir ventana modal
    Open Homepage
    Title Should Be    Hola Mundo!
    Set Focus To Element    xpath=/html/body/div[1]/div/img
    Click Link     xpath=/html/body/div[1]/div/div[2]/a[2]
    Title Should Be     Hola Mundo!
    Wait Until Element is Visible    xpath=//*[@id="exampleModal"]/div/div/div[3]/button[1]
    Close Browser
