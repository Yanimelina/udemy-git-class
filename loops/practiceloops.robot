***Settings***
Library     SeleniumLibrary
Library     String

***Variables***
${Browser}   Chrome
${Homepage}     automationpractice.com/index.php
${Scheme}       http
${TestURL}      ${Scheme}://${Homepage}

***Keywords***
Open Homepage
    Open Browser   ${TestURL}    ${Browser}

***Test Cases***
C001 Hacer Click En Contenedores
    Open Homepage
    Set Global Variable     @{NombresDeLosContenedores}      //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR   ${NombreDeContenedor}     IN     @{NombresDeLosContenedores}
          Wait Until Element Is Visible       xpath=${nombreDeContenedor}
          Click Element   xpath= ${NombreDeContenedor}
          Wait Until Element is Visible    xpath= //*[@id="bigpic"]
           Click Element    xpath= //*[@id="header_logo"]/a/img
     END
        Close Browser